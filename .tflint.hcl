plugin "terraform" {
  enabled = true
  preset  = "all"
}

config {}

rule "terraform_documented_variables" {
    enabled = false
}

rule "terraform_documented_outputs" {
    enabled = false
}

rule "terraform_naming_convention" {
    enabled = false
}