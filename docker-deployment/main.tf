resource "kubernetes_deployment" "deployment" {
  metadata {
    name = var.app_name
    labels = {
      app = var.app_name
    }
    namespace = var.namespace
  }

  spec {
    replicas = var.replicas

    selector {
      match_labels = {
        app = var.app_name
      }
    }

    template {
      metadata {
        annotations = var.pod_annotations
        labels = {
          app = var.app_name
        }
      }

      spec {

        dynamic "image_pull_secrets" {
          for_each = var.image_pull_secret_name == null ? [] : [1]

          content {
            name = var.image_pull_secret_name
          }
        }


        container {
          image = var.image
          name  = var.app_name

          port {
            container_port = var.container_port
          }

          resources {
            requests = {
              cpu    = var.resources.requests.cpu
              memory = var.resources.requests.memory
            }
            limits = {
              cpu    = var.resources.limits.cpu
              memory = var.resources.limits.memory
            }
          }

          dynamic "env" {
            for_each = var.env_vars

            content {
              name  = env.value.name
              value = env.value.value
            }
          }

          dynamic "env" {
            for_each = var.secret_env_vars

            content {
              name = env.value.name
              value_from {
                secret_key_ref {
                  key  = env.value.secret_key
                  name = env.value.secret_name
                }
              }
            }
          }

          liveness_probe {
            http_get {
              path = var.liveness_probe.http_get.path
              port = var.liveness_probe.http_get.port

            }
            initial_delay_seconds = var.liveness_probe.initial_delay_seconds
            period_seconds        = var.liveness_probe.period_seconds
            timeout_seconds       = var.liveness_probe.timeout_seconds
          }

          readiness_probe {
            http_get {
              path = var.readiness_probe.http_get.path
              port = var.readiness_probe.http_get.port

            }
            initial_delay_seconds = var.readiness_probe.initial_delay_seconds
            period_seconds        = var.readiness_probe.period_seconds
            timeout_seconds       = var.readiness_probe.timeout_seconds
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "app" {
  metadata {
    name      = var.app_name
    namespace = var.namespace

    labels = {
      app = var.app_name
    }
  }

  spec {
    selector = {
      app = var.app_name
    }

    port {
      port        = var.service_port
      target_port = var.container_port
    }
  }

}

module "ingress" {
  count              = var.ingress_host != null ? 1 : 0
  source             = "../nginx-certmanager-tls-ingress/"
  app_name           = var.app_name
  host               = var.ingress_host
  letsencrypt_issuer = var.letsencrypt_issuer
  namespace          = var.namespace
  server_snippet     = var.ingress_server_snippet
  service_name       = kubernetes_service.app.metadata[0].name
  limit_rpm          = var.limit_rpm
  limit_whitelist    = var.limit_whitelist
  proxy_body_size    = var.proxy_body_size
  ingress_class      = var.ingress_class
  path               = var.ingress_path
}
