
variable "namespace" {
  type = string
}

variable "app_name" {
  type        = string
  description = "Name of the app. Must be unique across the namespace, because the service uses the name to find pods"
}

variable "replicas" {
  default     = "1"
  description = "Number of replices the deployment is targeting for"
  type        = number
}

variable "image" {
  type        = string
  description = "docker image to deploy"
}

variable "container_port" {
  default     = "80"
  description = "port the docker container is reachable at internally"
  type        = number
}

variable "service_port" {
  default     = "80"
  description = "port the service is reachable at within the cluster"
  type        = number
}

variable "letsencrypt_issuer" {
  type        = string
  description = "issuer to use for certificates of cert-manager"
}
variable "ingress_class" {
  type        = string
  description = "ingress class to specify in ingress"
  default     = "nginx"
}

variable "ingress_host" {
  type        = string
  default     = null
  description = "if not set, no ingress is created"
}


variable "ingress_path" {
  type    = string
  default = "/"
}

variable "ingress_server_snippet" {
  type        = string
  description = "Server snippet to hand over to Nginx. Useful e.g. to block locations (health, metrics) for all provided services."
  default     = ""
}

variable "resources" {
  default = {
    limits = {
      cpu    = "0.5"
      memory = "512Mi"
    }
    requests = {
      cpu    = "250m"
      memory = "50Mi"
    }
  }
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
}

variable "image_pull_secret_name" {
  default     = null
  type        = string
  description = "name of the secret containing secrets to the registry where the docker image resides"
}

variable "liveness_probe" {
  default = {
    http_get = {
      path = "/"
      port = 80
    }
    initial_delay_seconds = 3
    period_seconds        = 3
    timeout_seconds       = 1
  }
  type = object({
    http_get = object({
      path = string
      port = number
    })
    initial_delay_seconds = number
    period_seconds        = number
    timeout_seconds       = number
  })
}
variable "readiness_probe" {
  default = null
  type = object({
    http_get = object({
      path = string
      port = number
    })

    initial_delay_seconds = number
    period_seconds        = number
    timeout_seconds       = number
  })
}

variable "env_vars" {
  default = []
  type = list(object({
    name  = string
    value = string
  }))
}

variable "secret_env_vars" {
  default = []
  type = list(object({
    name        = string
    secret_name = string
    secret_key  = string
  }))
}

variable "limit_rpm" {
  type    = string
  default = "0"
}

variable "limit_whitelist" {
  type    = string
  default = ""
}

variable "proxy_body_size" {
  type    = string
  default = "1m"
}

variable "pod_annotations" {
  type        = map(string)
  default     = {}
  description = "Kubernetes annotations that will be added to every pod of this deployment"
}
