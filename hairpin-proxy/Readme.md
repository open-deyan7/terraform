## Hairpin Proxy Kubernetes Deployment

This set of Terraform resources deploys a [hairpin proxy](https://github.com/compumike/hairpin-proxy) on a Kubernetes cluster. The hairpin proxy is a containerized load balancer that allows the proxy to send requests to the same Kubernetes service it's running in, by manipulating the DNS response. The resources include a namespace, deployment for the haproxy container, a service for the haproxy container, service account, cluster role, cluster role binding, role and role binding for the controller. 

## Why do i need this
If you have a cloud load balancer with PROXY protocol enabled, like nginx and the self-checks of cert-manager (challenges) are failing and no certificates issued. This suggests that there may be a problem with the HTTP connection, as the self-check likely requires a successful HTTP connection to complete. 
NGINX expects the PROXY protocol. The haproxy appends --haproxy-protocol to requests, so that the traffic is being rewritten by Kubernetes to bypass the external load balancer. 

# Setup

Make sure, that the ingress-nginx-controller Service within the ingress-nginx namespace matches the default value of `var.hairpin_proxy_ingress_controller_target_server`, or configure your own.


