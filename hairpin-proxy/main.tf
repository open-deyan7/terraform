resource "kubernetes_namespace" "hairpin_proxy" {
  metadata {
    name = "hairpin-proxy"
  }
}

resource "kubernetes_deployment" "hairpin_proxy_haproxy" {
  metadata {
    name      = "hairpin-proxy-haproxy"
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name

    labels = {
      app = "hairpin-proxy-haproxy"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "hairpin-proxy-haproxy"
      }
    }

    template {
      metadata {
        labels = {
          app = "hairpin-proxy-haproxy"
        }
      }

      spec {
        container {
          image = "compumike/hairpin-proxy-haproxy:${var.hairpin_proxy_version}"
          name  = "main"
          env {
            name  = "TARGET_SERVER"
            value = var.hairpin_proxy_ingress_controller_target_server
          }

          resources {
            limits = {
              cpu    = "50m"
              memory = "200Mi"
            }

            requests = {
              cpu    = "10m"
              memory = "100Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "hairpin_proxy" {
  metadata {
    name      = "hairpin-proxy"
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name
  }

  spec {
    selector = {
      app = kubernetes_deployment.hairpin_proxy_haproxy.metadata[0].labels.app
    }

    port {
      name        = "http"
      port        = 80
      target_port = 80
    }

    port {
      name        = "https"
      port        = 443
      target_port = 443
    }
  }
}

resource "kubernetes_service_account" "hairpin_proxy_controller" {
  metadata {
    name      = "hairpin-proxy-controller-sa"
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name
  }
}

resource "kubernetes_cluster_role" "hairpin_proxy_controller" {
  metadata {
    name = "hairpin-proxy-controller-cr"
  }

  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "hairpin_proxy_controller" {
  metadata {
    name = "hairpin-proxy-controller-crb"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.hairpin_proxy_controller.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.hairpin_proxy_controller.metadata[0].name
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name
  }
}

resource "kubernetes_role" "hairpin_proxy_controller" {
  metadata {
    name      = "hairpin-proxy-controller-r"
    namespace = "kube-system"
  }

  rule {
    api_groups     = [""]
    resources      = ["configmaps"]
    resource_names = ["coredns"]
    verbs          = ["get", "watch", "update"]
  }
}

resource "kubernetes_role_binding" "hairpin_proxy_controller" {
  metadata {
    name      = "hairpin-proxy-controller-rb"
    namespace = "kube-system"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.hairpin_proxy_controller.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.hairpin_proxy_controller.metadata[0].name
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name
  }
}

resource "kubernetes_deployment" "hairpin_proxy_controller" {
  metadata {
    name      = "hairpin-proxy-controller"
    namespace = kubernetes_namespace.hairpin_proxy.metadata[0].name
    labels = {
      app = "hairpin-proxy-controller"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "hairpin-proxy-controller"
      }
    }

    template {
      metadata {
        labels = {
          app = "hairpin-proxy-controller"
        }
      }

      spec {
        service_account_name = kubernetes_service_account.hairpin_proxy_controller.metadata[0].name

        security_context {
          run_as_user  = 405
          run_as_group = 65533
        }

        container {
          image = "compumike/hairpin-proxy-controller:${var.hairpin_proxy_version}"
          name  = "main"

          resources {
            limits = {
              cpu    = "50m"
              memory = "100Mi"
            }

            requests = {
              cpu    = "10m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}
