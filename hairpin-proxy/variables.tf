variable "hairpin_proxy_version" {
  type        = string
  description = "Version of hairpin-proxy to deploy"
  default     = "0.2.1"
}

variable "hairpin_proxy_ingress_controller_target_server" {
  type        = string
  description = "Target server for hairpin-proxy ingress controller"
  default     = "nginx-ingress-controller.ingress-nginx.svc.cluster.local"
}
