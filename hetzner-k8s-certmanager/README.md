# Hetzner K8S Certmanager

Creates a cert-manager deployment that can manage letsencrypt certificates in a hetzner kubernetes cluster that works
behind a class 4 load balancer with active proxy protocol.

In that scenario there are some issues with a default cert manager deployment, that are adressed by using special
container for the acme challenge of lets encrypt

## Reason for this module

This module depends on the k8s cluster being ready, and the providers being ready. It needs to be created outside of the
cluster module, because otherwise there is a cyclic dependencies which makes removing the cluster impossible.

## When removing a cluster

When removing a cluster, you first need to remove all resources created on that cluster. Only then the cluster + the
providers derived from the cluster can be removed in a second apply iteration.