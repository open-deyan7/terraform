
resource "helm_release" "cert_manager" {
  name = "cert-manager"

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = kubernetes_namespace.cert-manager.metadata[0].name
  version    = "v1.4.1"
  values = [
    <<YAML
image:
  repository: jyrno42/cert-manager-controller
  tag: v1.4.1

extraArgs:
  - "--acme-http01-solver-image=jyrno42/cert-manager-acmesolver:v1.3.1"
installCRDs: true
YAML
  ]

}

