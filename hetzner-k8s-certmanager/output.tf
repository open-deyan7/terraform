output "letsencrypt_issuer_production" {
  value = "letsencrypt-prod"
}
output "letsencrypt_issuer_staging" {
  # Use this issuer to not e.g. run into rate limits, when cluster is re-created several times https://letsencrypt.org/docs/staging-environment/
  value = "letsencrypt-staging"
}
