terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.13.0, < 2.0.0"

    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0, < 3.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.6.1, < 3.0.0"
    }
  }
}
