variable "letsencrypt_email" {
  type        = string
  description = "email address used when registring letsencrypt certificate. This email will receive expiration notifications."
}
