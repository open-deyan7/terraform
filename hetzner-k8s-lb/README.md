# K8s Cluster with terraform

Features
- K8S Cluster
- Load Balancer uses proxy protocol to resolve real ip address

## k8s cluster setup
Based on using https://github.com/JWDobken/terraform-hcloud-kubernetes / https://registry.terraform.io/modules/JWDobken/kubernetes/hcloud/latest

## How to run

- Create passwordless ssh key pair tf_hetzner by executing `ssh-keygen -f ~/.ssh/tf_hetzner -t rsa -b 4096 -N ''`
- Add it to your ssh agent using `ssh-add tf_hetzner`, so terraform can access the private key
- Create a Hetzner project and specify the hcloud_token variable, for instance exporting `TF_VAR_hcloud_token=<YOUR_TOKEN>` on the command line  or any other valid way to specify terraform variables
