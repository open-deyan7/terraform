module "hcloud_kubernetes_cluster" {
  source             = "JWDobken/kubernetes/hcloud"
  version            = "0.3.1"
  cluster_name       = var.cluster_name
  hcloud_token       = var.hcloud_token
  hcloud_ssh_keys    = [hcloud_ssh_key.test-cluster.id]
  control_plane_type = "cx11" # optional
  worker_type        = "cx21" # optional
  worker_count       = var.worker_count
}
