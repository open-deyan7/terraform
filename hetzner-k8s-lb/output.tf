output "endpoint" {
  value = module.hcloud_kubernetes_cluster.endpoint
}

output "certificate_authority_data" {
  value = module.hcloud_kubernetes_cluster.certificate_authority_data
}
output "client_certificate_data" {
  value = module.hcloud_kubernetes_cluster.client_certificate_data
}
output "client_key_data" {
  value = module.hcloud_kubernetes_cluster.client_key_data
}
output "ingress_load_balancer_ipv4" {
  value = hcloud_load_balancer.load_balancer.ipv4
}

output "ingress_load_balancer_name" {
  value = hcloud_load_balancer.load_balancer.name
}

output "kubeconfig" {
  value = module.hcloud_kubernetes_cluster.kubeconfig
}
