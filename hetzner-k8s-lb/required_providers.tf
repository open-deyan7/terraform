terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.35.2, < 2.0.0"
    }
  }
}
