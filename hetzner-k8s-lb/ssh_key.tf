resource "hcloud_ssh_key" "test-cluster" {
  name       = "tf_hetzner"
  public_key = file(var.ssh_public_key_path)
}