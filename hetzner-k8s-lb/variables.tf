variable "hcloud_token" {
  type        = string
  description = "hetzner api token for the project which should contain cluster resources"
}

variable "cluster_name" {
  type = string
}

variable "ssh_public_key_path" {
  type        = string
  description = "public key of the ssh key used to provision hetzner servers. The private key needs to be added to ssh agent"
}
variable "worker_count" {
  type        = number
  description = "number of workers to use."
}
