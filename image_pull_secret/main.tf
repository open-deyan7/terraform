
resource "kubernetes_secret" "regsecret" {
  metadata {
    name      = var.secret_name
    namespace = var.namespace
  }

  data = {
    ".dockerconfigjson" = jsonencode({
      "auths" : {
        (var.docker_server) : {
          email    = var.docker_email
          username = var.docker_username
          password = var.docker_password
        }
      }
    })
  }

  type = "kubernetes.io/dockerconfigjson"
}