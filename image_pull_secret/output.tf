output "secret_name" {
  value = kubernetes_secret.regsecret.metadata[0].name
}
output "namespace" {
  value = kubernetes_secret.regsecret.metadata[0].namespace
}
