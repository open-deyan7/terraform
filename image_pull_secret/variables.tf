variable "docker_email" {
  type = string
}
variable "docker_username" {
  type = string
}
variable "docker_password" {
  type = string
}
variable "docker_server" {
  type = string
}
variable "namespace" {
  type = string
}
variable "secret_name" {
  type = string
}
