# Hetzner K8S Ingress NGINX

Creates a nginx-ingress deployment that will use a hetzner load balancer with activated proxy protocol. This means, the
real ip address can be resolved, such that rate limiting can be activated.