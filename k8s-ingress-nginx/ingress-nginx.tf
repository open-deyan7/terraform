
resource "helm_release" "nginx_ingress" {
  name = "nginx-ingress-controller"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"
  version    = var.chart_version
  namespace  = kubernetes_namespace.ingress-nginx.metadata[0].name
  values = [
    <<YAML
config:
    use-proxy-protocol: "true"
    ${length(var.extra_config) == 0 ? "" : indent(4, yamlencode(var.extra_config))}
service:
  annotations:
    load-balancer.hetzner.cloud/name: "${var.load_balancer_name}"
    load-balancer.hetzner.cloud/uses-proxyprotocol: "true"
metrics:
    enabled: ${var.metrics_enabled}
    service:
        annotations:
            prometheus.io/path: /metrics
            prometheus.io/port: "10254"
            prometheus.io/scrape: "${var.metrics_enabled}"

YAML
  ]

}
