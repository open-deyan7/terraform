terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0, < 3.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.8.0, < 3.0.0"
    }
  }
}
