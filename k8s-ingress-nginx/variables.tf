variable "load_balancer_name" {
  type        = string
  description = "name of the kubernetes load balancer to use for ingress"
}

variable "metrics_enabled" {
  type        = bool
  default     = false
  description = "enables the Nginx Ingress Controller metrics"
}

variable "chart_version" {
  type        = string
  default     = "9.3.26"
  description = "Sets the Nginx Ingress Controller chart version"
}

variable "extra_config" {
  type        = map(string)
  default     = {}
  description = "Extends the Nginx Ingress Controller default config map"
}