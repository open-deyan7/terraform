## Howto

### Upgrade from 15.0.2 to 18.0.0

#### Prerequisites

- You have run a successful backup job to your backup location
- The old keycloak is using the same postgres version as keycloak-x (likely if using the terraform module of this module collection)

#### Steps

- Add keycloak-x module next to your existing keycloak instance, for instance like this

```terraform
module "keycloak-18" {
  source                       = "git::https://gitlab.com/open-deyan7/terraform.git//keycloak-x?ref=9f5e28a3644667233eba160f5702a8f74e60273a"
  backup_aws_access_key        = var.aws_access_key
  backup_aws_region            = var.backup_bucket_location
  backup_aws_s3_bucket         = var.backup_bucket_name
  backup_aws_secret_key        = var.aws_secret_key
  backup_schedule              = var.keycloak_backend_backup_schedule
  ingress_host                 = "${var.keycloak_18_ingress_prefix}.${var.cluster_domain}"
  letsencrypt_issuer           = var.letsencrypt_issuer
  namespace                    = "my-namespace"
  backup_encryption_passphrase = var.backup_encryption_passphrase
  restore_s3_object            = var.restore_keycloak_18_s3_object # should be different than the old keycloak restore version
  keycloak_version             = var.keycloak_version
  keycloak_image_tag           = var.keycloak_image_tag
  keycloak_suffix              = "-18" # important to avoid naming conflict with old keycloak
  keycloak_helm_name_suffix    = "-18" # important to avoid naming conflict with old keycloak
  keycloak_postgres_app_name   = "keycloak-18" # important to avoid naming conflict with old keycloak

}
```

- When this is running, set `restore_s3_object` to the old keycloak backup
- Restart the keycloak 18 pod
- Keycloak 18 now has the old data
- When everything is ready, switch ingress names of old and new keycloak
- When everything is working, remove the old keycloak instance