resource "random_password" "keycloak-postgres-password" {
  length  = 32
  special = false
}

resource "kubernetes_secret" "keycloak-secrets" {
  metadata {
    name      = "keycloak-secrets"
    namespace = var.namespace
  }

  data = {
    database_password = random_password.keycloak-postgres-password.result
    database_username = var.database_username
  }
}

module "keycloak-postgres" {
  source                       = "git::https://gitlab.com/open-deyan7/terraform.git//postgres?ref=f17aae8771a2d6f4a5f1c0592bd8d83bc5677608"
  app_name                     = var.keycloak_postgres_app_name
  backup_aws_enabled           = var.backup_aws_enabled
  backup_aws_access_key        = var.backup_aws_access_key
  backup_aws_region            = var.backup_aws_region
  backup_s3_bucket             = var.backup_aws_s3_bucket
  backup_aws_secret_key_id     = var.backup_aws_secret_key
  database_name                = var.database_name
  backup_schedule              = var.backup_schedule
  metrics_enabled              = var.metrics_enabled
  database_password            = kubernetes_secret.keycloak-secrets.data.database_password
  existing_secret              = var.existing_secret
  database_username            = kubernetes_secret.keycloak-secrets.data.database_username
  namespace                    = var.namespace
  backup_encryption_passphrase = var.backup_encryption_passphrase
  restore_s3_object            = var.restore_s3_object
  database_port                = var.keycloak_postgres_port
}

resource "helm_release" "keycloak" {
  name       = "keycloak${var.keycloak_helm_name_suffix}"
  chart      = "keycloakx"
  repository = "https://codecentric.github.io/helm-charts"
  namespace  = var.namespace
  version    = var.keycloak_version

  dynamic "set" {
    for_each = var.image_pull_secret_name == null ? [] : [1]

    content {
      name  = "imagePullSecrets[0].name"
      value = var.image_pull_secret_name
    }
  }


  values = concat(
    var.keycloak_helm_values,
    [
      <<YAML
      fullnameOverride: "keycloak${var.keycloak_suffix}"
      image:
        tag: "${var.keycloak_image_tag}"
      command:
        - "/opt/keycloak/bin/kc.sh"
        - "--verbose"
        - "start"
        - "--http-enabled=true"
        - "--http-port=${var.keycloak_port}"
        - "--metrics-enabled=${var.metrics_enabled}"
        - "--health-enabled=${var.health_enabled}"
        - "--hostname-strict=false"
        - "--hostname-strict-https=false"
        - "--spi-events-listener-jboss-logging-success-level=info"
        - "--spi-events-listener-jboss-logging-error-level=warn"
        - "--features=token-exchange"
      service:
        annotations:
      podAnnotations:
        prometheus.io/scrape: "${var.metrics_enabled}"
        prometheus.io/path: "/auth/metrics"
        prometheus.io/port: "${var.keycloak_port}"
      extraEnv: |
        - name: PROXY_ADDRESS_FORWARDING
          value: "true"
        - name: KEYCLOAK_STATISTICS
          value: all
        - name: JAVA_OPTS_APPEND
          value: >-
            -XX:+UseContainerSupport
            -XX:MaxRAMPercentage=50.0
            -Djava.awt.headless=true
            -Djgroups.dns.query={{ include "keycloak.fullname" . }}-headless
            -Djava.net.preferIPv4Stack=true
            -Dkeycloak.profile.feature.upload_scripts=enabled
            -Dkeycloak.profile.feature.token_exchange=enabled
        - name: KC_DB_USERNAME
          valueFrom:
            secretKeyRef: 
              name: ${kubernetes_secret.keycloak-secrets.metadata[0].name}
              key: database_username
        - name: KC_DB_PASSWORD
          valueFrom:
            secretKeyRef: 
              name: ${kubernetes_secret.keycloak-secrets.metadata[0].name}
              key: database_password
      extraInitContainers: |
        - name: theme-provider
          image: ${var.theme_image_location}
          imagePullPolicy: IfNotPresent
          command:
            - sh
          args:
            - -c
            - |
              echo "Copying theme..."
              cp -R /${var.theme_name}/* /theme
          volumeMounts:
            - name: theme
              mountPath: /theme

      extraVolumeMounts: |
        - name: theme
          mountPath: /opt/keycloak/themes/${var.theme_name}

      extraVolumes: |
        - name: theme
          emptyDir: {}
      
      dbchecker:
        enabled: true

      database:
        vendor: postgres
        hostname: ${var.keycloak_postgres_app_name}-postgresql
        port: ${var.keycloak_postgres_port}
        database: ${var.database_name}
YAML
  ])
}

module "keycloak-ingress" {
  source             = "git::https://gitlab.com/open-deyan7/terraform.git//nginx-certmanager-tls-ingress?ref=0b7fe53fe8ad4ddb9824cba4fe7bf50802318799"
  app_name           = "keycloak${var.keycloak_suffix}"
  host               = var.ingress_host
  letsencrypt_issuer = var.letsencrypt_issuer
  namespace          = var.namespace
  server_snippet     = var.ingress_server_snippet
  service_name       = "keycloak${var.keycloak_suffix}-http"
  limit_rpm          = var.limit_rpm
  limit_whitelist    = var.limit_whitelist
  proxy_body_size    = var.proxy_body_size
  proxy_buffer_size  = "16k"
}
