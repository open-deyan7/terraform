#defines which providers are required by the main module

terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = ">= 3.4.3, < 4.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.8.0, < 3.0.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0, < 3.0.0"
    }
  }
}
