variable "namespace" {
  type = string
}
variable "ingress_host" {
  type = string
}

variable "ingress_server_snippet" {
  type        = string
  description = "Server snippet to hand over to Nginx. Useful e.g. to block locations (health, metrics) for all provided services."
  default     = "location /auth/metrics { deny all; }"
}

variable "letsencrypt_issuer" {
  type = string
}

variable "restore_s3_object" {
  type        = string
  description = "if set, this object will be restored to the keycloak database once. Must point to a .dump file created by pg_restore -Fc"
  default     = null
}

variable "backup_aws_enabled" {
  description = "Enables or disables the AWS backup functionality for the database"
  type        = bool
  default     = true
}

variable "backup_aws_access_key" {
  type = string
}
variable "backup_aws_secret_key" {
  type = string
}
variable "backup_aws_region" {
  type = string
}
variable "backup_aws_s3_bucket" {
  type = string
}

variable "backup_encryption_passphrase" {
  type        = string
  description = "the backup is encrypted and restored using aes-256-cbc with this symmetric key"
}

variable "backup_schedule" {
  type        = string
  default     = "0 3 * * *"
  description = "cron expression which describes when to execute the backup"
}

variable "image_pull_secret_name" {
  type        = string
  default     = ""
  description = "name of a potential image pull secret that is required to get additional containers"
}

variable "keycloak_helm_values" {
  default = [
    <<YAML
    resources:
      limits:
        cpu: 1
        memory: 512Mi
      requests:
        cpu: 200m
        memory: 384Mi
YAML
  ]
  type        = list(string)
  description = "can be used to further customize helm values"
}

variable "keycloak_suffix" {
  type    = string
  default = ""
}

variable "keycloak_helm_name_suffix" {
  type        = string
  default     = ""
  description = "suffix to append to the helm instance name. This is necessary if another module already uses the same name."
}

variable "keycloak_version" {
  type    = string
  default = "1.3.1"
}

variable "keycloak_image_tag" {
  type        = string
  default     = ""
  description = "Overrides the Keycloak image tag whose default is the chart version."
}

variable "keycloak_port" {
  default     = 8080
  type        = number
  description = "Sets the port where Keycloak is being served"
}

variable "keycloak_postgres_app_name" {
  type    = string
  default = "keycloak"
}

variable "keycloak_postgres_port" {
  type    = number
  default = 5432
}

variable "database_username" {
  type    = string
  default = "keycloak"
}

variable "database_name" {
  type    = string
  default = "keycloak"
}

variable "theme_image_location" {
  type    = string
  default = "registry.gitlab.com/open-deyan7/keycloak-custom-theme-container:842f257674a9f6d1550325235cc4fe2201bd4353"
}

variable "theme_name" {
  type    = string
  default = "custom-theme"
}

variable "limit_rpm" {
  type        = string
  description = "the requests per minute that influence rate limiting. 0 means deactivated"
  default     = "0"
}

variable "limit_whitelist" {
  type    = string
  default = ""
}

variable "proxy_body_size" {
  type    = string
  default = "1m"
}

variable "metrics_enabled" {
  type    = bool
  default = false
}

variable "health_enabled" {
  type    = bool
  default = false
}

variable "existing_secret" {
  type        = string
  default     = ""
  description = "Existing secret for PostgreSQL authentication"
}