


resource "kubernetes_secret" "keycloak-credentials" {
  metadata {
    name      = "keycloak-credentials"
    namespace = var.namespace
  }

  data = {
    keycloak_username = (var.initial_admin_username)
    keycloak_password = (var.initial_admin_password)
  }
}

resource "helm_release" "keycloak" {
  name             = "keycloak"
  chart            = "keycloak"
  repository       = "https://codecentric.github.io/helm-charts"
  namespace        = var.namespace
  version          = var.keycloak_version
  imagePullSecrets = [var.image_pull_secret_name]
  values = concat(
    var.keycloak_helm_values,
    [
      <<YAML
      fullnameOverride: "keycloak${var.keycloak_suffix}"
      image:
        tag: "${var.keycloak_image_tag}"
      service:
        annotations:
      extraEnv: |
        - name: KEYCLOAK_USER_FILE
          value: /secrets/keycloak-user-creds/keycloak_username
        - name: KEYCLOAK_PASSWORD_FILE
          value: /secrets/keycloak-user-creds/keycloak_password
        - name: PROXY_ADDRESS_FORWARDING
          value: "true"
        - name: KEYCLOAK_STATISTICS
          value: all
        - name: JAVA_OPTS
          value: >-
            -XX:+UseContainerSupport
            -XX:MaxRAMPercentage=50.0
            -Djava.net.preferIPv4Stack=true
            -Djboss.modules.system.pkgs=$JBOSS_MODULES_SYSTEM_PKGS
            -Djava.awt.headless=true
            -Dkeycloak.profile.feature.upload_scripts=enabled
            -Dkeycloak.profile.feature.token_exchange=enabled
      extraVolumeMounts: |
        - name: keycloak-user-creds
          mountPath: /secrets/keycloak-user-creds
          readOnly: true
        - name: theme
          mountPath: /opt/jboss/keycloak/themes/portabiles
      extraVolumes: |
        - name: keycloak-user-creds
          secret:
            secretName: keycloak-credentials
        - name: theme
          emptyDir: {}
YAML
  ])
}

module "keycloak-ingress" {
  source             = "git::https://gitlab.com/open-deyan7/terraform.git//nginx-certmanager-tls-ingress?ref=7fe939fda6c48652f1cd0988ecac0c371ba6be6e"
  app_name           = "keycloak"
  host               = var.ingress_host
  letsencrypt_issuer = var.letsencrypt_issuer
  namespace          = var.namespace
  service_name       = "keycloak-http"
  limit_rpm          = var.limit_rpm
  limit_whitelist    = var.limit_whitelist
  proxy_body_size    = var.proxy_body_size
}

module "keycloak-backup-restore" {
  depends_on        = [helm_release.keycloak]
  source            = "git::https://gitlab.com/open-deyan7/terraform.git//postgres_backup_restore?ref=494c7cf019ddad6bed5a4775b589d85d6db17478"
  app_name          = "keycloak"
  database_address  = "keycloak-postgresql"
  database_name     = "keycloak"
  database_password = "keycloak"
  database_user     = "keycloak"
  namespace         = var.namespace
  aws_access_key    = var.backup_aws_access_key
  aws_secret_key_id = var.backup_aws_secret_key
  aws_region        = var.backup_aws_region
  s3_bucket         = var.backup_aws_s3_bucket

  schedule              = var.backup_schedule
  s3_object             = var.restore_s3_object
  encryption_passphrase = var.backup_encryption_passphrase
}
