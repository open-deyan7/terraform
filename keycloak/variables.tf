
variable "ingress_host" {
  type = string
}
variable "letsencrypt_issuer" {
  type = string
}

variable "restore_s3_object" {
  type        = string
  description = "if set, this object will be restored to the keycloak database once. Must point to a .dump file created by pg_restore -Fc"
  default     = null
}


variable "namespace" {
  type = string
}
variable "backup_aws_access_key" {
  type = string
}
variable "backup_aws_secret_key" {
  type = string
}
variable "backup_aws_region" {
  type = string
}
variable "backup_aws_s3_bucket" {
  type = string
}
variable "keycloak_suffix" {
  type    = string
  default = ""
}
variable "keycloak_version" {
  type    = string
  default = "15.0.1"
}
variable "initial_admin_username" {
  type    = string
  default = "admin"
}
variable "initial_admin_password" {
  type    = string
  default = "admin"
}

variable "image_pull_secret_name" {
  type        = string
  default     = ""
  description = "name of a potential image pull secret that is required to get additional containers"
}

variable "keycloak_helm_values" {
  default     = []
  type        = list(string)
  description = "can be used to further customize helm values"
}

variable "backup_encryption_passphrase" {
  type        = string
  description = "the backup is encrypted and restored using aes-256-cbc with this symmetric key"
}
variable "backup_schedule" {
  type        = string
  default     = "0 3 * * *"
  description = "cron expression which describes when to execute the backup"
}

variable "keycloak_image_tag" {
  type        = string
  default     = ""
  description = "Overrides the Keycloak image tag whose default is the chart version."
}

variable "limit_rpm" {
  type    = string
  default = "0"
}

variable "limit_whitelist" {
  type    = string
  default = ""
}

variable "proxy_body_size" {
  type    = string
  default = "1m"
}
