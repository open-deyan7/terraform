# Monitoring with Grafana, Loki and Prometheus
This module was created by being inspired from https://www.scaleway.com/en/docs/tutorials/monitor-k8s-grafana/ and https://www.scaleway.com/en/docs/tutorials/manage-k8s-logging-loki/.

Instead of the standalone Grafana installation as described in the tutorial above, we are using https://github.com/grafana-operator/grafana-operator via the Helm chart https://artifacthub.io/packages/helm/bitnami/grafana-operator/ to deploy Grafana as an Operator, enabling the configuration via Custom Resource Definitions (CRD) directly as K8s manifests.  

After deploying, Grafana, Loki and Prometheus are ready to go and already configured to work together.  

## IMPORTANT: close down ports
By default, the DaemonSet `prometheus-node-exporter` opens a `hostPort` for TCP 9100. This will be publicly available on your public Node IPs, so it´s recommended to close them down via a firewall rule or similar. For Scaleway, it looks like this and can also be configured via IaC `SecurityGroup`s:  
(replace `10.18.0.0/15` with your cluster internal IP range)  
```
resource "scaleway_instance_security_group_rules" "deny_node_exporter_metrics" {
  security_group_id = scaleway_instance_security_group.default-security-group.id

  inbound_rule {
    action   = "accept"
    port     = 9100
    protocol = "TCP"
    ip_range = "10.18.0.0/15" #to allow metrics fetching from one Node to the other
  }

  inbound_rule {
    action   = "drop"
    port     = 9100
    protocol = "TCP"
  }
}
```

## Access to Grafana
- Establish a port-forward to service `grafana-service`, port 3000.

The admin password can be found at:
```
kubectl get secret --namespace monitoring grafana-admin-credentials -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode; echo
```

## Add Dashboards or browse logs
Now you can browse logs or add Dashboards. It´s helpful to have a look at https://grafana.com/grafana/dashboards/ where there are loads of predefined dashboards that can be imported.  

We recommend to add any new Dashboards as CRDs, as can be seen in `dashboards.tf` or https://github.com/grafana-operator/grafana-operator/tree/master/deploy/examples/dashboards.

## Enable Prometheus scraping from custom metrics endpoints
If you enable metrics e.g. on your backend deployment, it is sufficient to add an annotation to the StatefulSet / Deployment / Pod / Service of your backend (whichever is more convenient). Prometheus will pick it up and start scraping instantly.  
If the scraping is successful can be checked by port-forwarding `prometheus-server` and then checking at http://localhost:9090/targets.
```
kind: Pod
metadata:
  annotations:
    prometheus.io/path: /metrics
    prometheus.io/port: "80"
    prometheus.io/scrape: "true"
```

## Add Alert Rules
Currently alert rules have to be imported manually. The standard set of rules is located in `alert-rule-configs/alert-rules.yaml`.