resource "kubectl_manifest" "grafana_node_exporter_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: node-exporter-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/1860/revisions/latest/download
    customFolderName: ${local.default_folder_name}
  YAML
}

resource "kubectl_manifest" "grafana_nginx_ingress_controller_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: nginx-ingress-controller-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/9614/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_loki_stack_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: loki-stack-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/14055/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_storage_volumes_namespace_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: k8s-storage-volumes-namespace-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/11455/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_coredns_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: coredns-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/14981/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_k8s_cluster_monitoring_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: k8s-cluster-monitoring-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/10000/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_k8s_postgres_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: k8s-postgres-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    url: https://grafana.com/api/dashboards/9628/revisions/latest/download
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
  YAML
}

resource "kubectl_manifest" "grafana_backend_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: backend-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
    - inputName: DS_LOKI
      datasourceName: Loki
    json: ${jsonencode(file("${path.module}/dashboard-configs/backend-dashboard.json"))}
  YAML
}

resource "kubectl_manifest" "grafana_keycloak_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: keycloak-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
    - inputName: DS_LOKI
      datasourceName: Loki
    json: ${jsonencode(file("${path.module}/dashboard-configs/keycloak-dashboard.json"))}
  YAML
}

resource "kubectl_manifest" "grafana_k8s_events_dashboard" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDashboard
  metadata:
    name: k8s-events-dashboard
    namespace: ${var.namespace}
    labels:
      app.kubernetes.io/instance: grafana-operator
  spec:
    customFolderName: ${local.default_folder_name}
    datasources:
    - inputName: DS_PROMETHEUS
      datasourceName: Prometheus
    - inputName: DS_LOKI
      datasourceName: Loki
    json: ${jsonencode(file("${path.module}/dashboard-configs/k8s-event-exporter.json"))}
  YAML
}
