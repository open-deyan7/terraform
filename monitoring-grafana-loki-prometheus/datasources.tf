resource "kubectl_manifest" "grafana_datasources" {
  depends_on = [helm_release.grafana_operator]
  yaml_body  = <<YAML
  apiVersion: integreatly.org/v1alpha1
  kind: GrafanaDataSource
  metadata:
    name: grafana-datasources-prometheus
    namespace: ${var.namespace}
  spec:
    datasources:
    - access: proxy
      editable: false
      jsonData:
        timeInterval: 15s
        tlsSkipVerify: false
      name: Prometheus
      type: prometheus
      url: http://prometheus-server
      version: 1
    - access: proxy
      editable: false
      isDefault: true
      jsonData: {}
      name: Loki
      type: loki
      url: http://loki-stack:3100
      version: 1
    name: datasources.yaml
  YAML
}
