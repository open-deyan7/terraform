resource "helm_release" "kubernetes_event_exporter" {
  name             = "kubernetes-event-exporter"
  chart            = "kubernetes-event-exporter"
  version          = var.kubernetes_event_exporter_chart_version
  repository       = "https://charts.bitnami.com/bitnami"
  namespace        = var.namespace
  create_namespace = true

  values = [
    <<YAML
    resources:
        limits:
            cpu: ${var.event_exporter_resources.limits.cpu}
            memory: ${var.event_exporter_resources.limits.memory}
        requests:
            cpu: ${var.event_exporter_resources.requests.cpu}
            memory: ${var.event_exporter_resources.requests.memory}
    config:
        logLevel: ${var.k8s_event_exporter_log_level}
        receivers:
        - name: dump
          file:
            path: "/dev/stdout"
            layout: 
                message: "{{ .Message }}"
                reason: "{{ .Reason }}"
                type: "{{ .Type }}"
                count: "{{ .Count }}"
                kind: "{{ .InvolvedObject.Kind }}"
                name: "{{ .InvolvedObject.Name }}"
                namespace: "{{ .Namespace }}"
                component: "{{ .Source.Component }}"
                host: "{{ .Source.Host }}"
                lastTimestamp: "{{ .LastTimestamp }}"
                firstTimestamp: "{{ .FirstTimestamp }}"
    YAML
  ]
}
