locals {
  default_folder_name = "Default"
  alert_rules_path    = var.alert_rules_file_path != null ? var.alert_rules_file_path : "${path.module}/alert-rule-configs/alert-rules.yaml"
}