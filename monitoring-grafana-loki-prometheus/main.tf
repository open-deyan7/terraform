resource "helm_release" "loki-stack" {
  name             = "loki-stack"
  chart            = "loki-stack"
  version          = var.loki_stack_chart_version
  repository       = "https://grafana.github.io/helm-charts"
  namespace        = var.namespace
  create_namespace = true

  values = [
    <<YAML
    promtail:
        enabled: true
    loki:
        persistence:
            enabled: true
            size: ${var.loki_persistence_size}
        config:
            limits_config:
                retention_period: ${var.loki_retention_period}
                max_query_series: ${var.max_query_series}
            compactor:
                delete_request_cancel_period: ${var.loki_delete_request_cancel_period}
                retention_enabled: ${var.loki_retention_enabled}
                retention_delete_delay: ${var.loki_retention_delete_delay}
            table_manager:
                retention_deletes_enabled: true
                retention_period: ${var.loki_retention_period}
            query_scheduler:
                max_outstanding_requests_per_tenant: ${var.loki_max_outstanding_requests_per_tenant}
    YAML
  ]
}

resource "helm_release" "prometheus" {
  name             = "prometheus"
  chart            = "prometheus"
  version          = var.prometheus_chart_version
  repository       = "https://prometheus-community.github.io/helm-charts"
  namespace        = var.namespace
  create_namespace = true

  values = [
    <<YAML
    server:
        extraArgs:
            storage.tsdb.retention.size: ${var.prometheus_retention_size}
        persistentVolume: 
            size: ${var.prometheus_persistence_size}
        retention: ${var.prometheus_retention_period}
    YAML
  ]
}

resource "helm_release" "grafana_operator" {
  name             = "grafana-operator"
  chart            = "grafana-operator"
  version          = var.grafana_operator_chart_version
  repository       = "https://charts.bitnami.com/bitnami"
  namespace        = var.namespace
  create_namespace = true
  wait             = true

  values = [
    <<-YAML
    operator:
      resources:
        limits:
            cpu: ${var.operator_resources.limits.cpu}
            memory: ${var.operator_resources.limits.memory}
        requests:
            cpu: ${var.operator_resources.requests.cpu}
            memory: ${var.operator_resources.requests.memory}
    grafana:
      ${var.enable_alert_rules ? indent(2, <<-YAML
      configMaps: 
        - alert-rules
      extraVolumeMounts:
        - name: alert-rules
          mountPath: /etc/grafana/provisioning/alerting/
      extraVolumes:
        - name: alert-rules
          configMap:
            name: alert-rules
      YAML
  ) : ""
}
      config:
        database:
          wal: ${var.enable_sqlite_only_wal}
          ${length(var.grafana_database_config) == 0 ? "" : indent(6, yamlencode(var.grafana_database_config))}
        alerting:
          enabled: ${var.enable_alert_rules}
        auth:
          login_maximum_inactive_lifetime_duration: ${var.login_lifetime.maximum_inactive_duration}
          login_maximum_lifetime_duration: ${var.login_lifetime.maximum_lifetime_duration}
        smtp:
          enabled: ${var.smtp.enabled}
          host: ${var.smtp.host}:${var.smtp.port}
          user: ${var.smtp.user}
          password: ${var.smtp_password}
          from_address: ${var.smtp.from_address}
          from_name: ${var.smtp.from_name}
        server:
          root_url: ${var.grafana_root_url}
      resources:
        limits:
            cpu: ${var.grafana_resources.limits.cpu}
            memory: ${var.grafana_resources.limits.memory}
        requests:
            cpu: ${var.grafana_resources.requests.cpu}
            memory: ${var.grafana_resources.requests.memory}
      persistence:
        size: ${var.grafana_persistence_size}
        type: pvc
        enabled: true

    YAML
]
}

resource "kubernetes_config_map" "alert-rules" {
  count = var.enable_alert_rules ? 1 : 0
  metadata {
    name      = "alert-rules"
    namespace = var.namespace
  }

  data = {
    "alert-rules.yaml" = file(local.alert_rules_path)
  }
}
