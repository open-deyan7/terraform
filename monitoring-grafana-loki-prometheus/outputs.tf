output "grafana_service_name" {
  value = "grafana-service"
}

output "grafana_service_port" {
  value = 3000
}
