variable "namespace" {
  default = "monitoring"
  type    = string
}

variable "loki_stack_chart_version" {
  default = "2.8.9"
  type    = string
}

variable "loki_persistence_size" {
  type    = string
  default = "10Gi"
}

variable "loki_retention_period" {
  type    = string
  default = "720h"
}

variable "grafana_operator_chart_version" {
  default = "2.7.24"
  type    = string
}

variable "grafana_persistence_size" {
  default = "10Gi"
  type    = string
}

variable "grafana_resources" {
  type = object({
    limits = object({
      cpu : string
      memory : string
    })
    requests = object({
      cpu : string
      memory : string
    })
  })
  default = {
    limits = {
      cpu    = "1000m"
      memory = "200Mi"
    }
    requests = {
      cpu    = "100m"
      memory = "50Mi"
    }
  }
}

variable "operator_resources" {
  type = object({
    limits = object({
      cpu : string
      memory : string
    })
    requests = object({
      cpu : string
      memory : string
    })
  })
  default = {
    limits = {
      cpu    = "200m"
      memory = "200Mi"
    }
    requests = {
      cpu    = "20m"
      memory = "20Mi"
    }
  }
}

variable "prometheus_chart_version" {
  default = "19.3.3"
  type    = string
}

variable "prometheus_persistence_size" {
  default = "10Gi"
  type    = string
}

variable "prometheus_retention_size" {
  default = "0"
  type    = string
}

variable "prometheus_retention_period" {
  default = "720h"
  type    = string
}

variable "login_lifetime" {
  type = object({
    maximum_inactive_duration : string
    maximum_lifetime_duration : string
  })
  default = {
    maximum_inactive_duration = "2d"
    maximum_lifetime_duration = "5d"
  }
}

variable "smtp" {
  type = object({
    enabled : bool
    host : string
    port : number
    user : string
    from_address : string
    from_name : string
  })
  default = {
    enabled      = false
    from_address = ""
    from_name    = ""
    host         = ""
    port         = 587
    user         = ""
  }
}

variable "smtp_password" {
  sensitive = true
  type      = string
  default   = ""
}

variable "grafana_root_url" {
  type        = string
  description = "This will appear in emails or is used when performing logins via Google etc."
}

variable "kubernetes_event_exporter_chart_version" {
  default = "2.1.6"
  type    = string
}

variable "event_exporter_resources" {
  type = object({
    limits = object({
      cpu : string
      memory : string
    })
    requests = object({
      cpu : string
      memory : string
    })
  })
  default = {
    limits = {
      cpu    = "100m"
      memory = "100Mi"
    }
    requests = {
      cpu    = "10m"
      memory = "10Mi"
    }
  }
}

variable "k8s_event_exporter_log_level" {
  type = string
  validation {
    condition     = contains(["fatal", "error", "warn", "info", "debug"], var.k8s_event_exporter_log_level)
    error_message = "Value did not match any of [fatal|error|warn|info|debug]"
  }
  default = "warn"
}

variable "loki_max_outstanding_requests_per_tenant" {
  type    = number
  default = 2048
}
variable "alert_rules_file_path" {
  type    = string
  default = null
}

variable "enable_alert_rules" {
  type    = bool
  default = false
}

variable "enable_sqlite_only_wal" {
  type    = bool
  default = false
}

variable "loki_delete_request_cancel_period" {
  type    = string
  default = "10m"
}

variable "loki_retention_enabled" {
  type    = bool
  default = true
}

variable "loki_retention_delete_delay" {
  type    = string
  default = "2h"
}

variable "grafana_database_config" {
  type        = map(string)
  default     = {}
  description = "Extends the Grafana default database config. enable_sqlite_only_wal has to be disabled in case its not sqlite"
}


variable "max_query_series" {
  type    = number
  default = 500
}