# Basic Auth Ingress
This module provides an Ingress that is secured by Basic Auth.  
The password is auto-created and can be fetched after appyling by executing:
```
kubectl get secret --namespace $NAMESPACE$ $APP_NAME$-basic-auth-secret -o jsonpath="{.data.secret}" | base64 --decode; echo
```
