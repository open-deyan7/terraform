
resource "kubernetes_ingress_v1" "app-ingress" {
  metadata {
    name = var.app_name
    annotations = {
      "cert-manager.io/cluster-issuer"              = var.letsencrypt_issuer,
      "kubernetes.io/ingress.class"                 = var.ingress_class,
      "nginx.ingress.kubernetes.io/limit-rpm"       = var.limit_rpm,
      "nginx.ingress.kubernetes.io/limit-whitelist" = var.limit_whitelist,
      "nginx.ingress.kubernetes.io/proxy-body-size" = var.proxy_body_size,
      "nginx.ingress.kubernetes.io/server-snippet"  = var.server_snippet
    }
    namespace = var.namespace
  }
  spec {

    rule {

      host = var.host
      http {
        path {

          backend {
            service {
              name = var.service_name
              port {
                number = var.service_port
              }
            }

          }

          path = var.path
        }
      }

    }
    tls {
      hosts       = [var.host]
      secret_name = "${var.app_name}-ingress-tls-certificate"
    }
  }

}