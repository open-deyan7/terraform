
variable "namespace" {
  type = string
}

variable "app_name" {
  type        = string
  description = "Name of the app. Must be unique across the namespace, because the service uses the name to find pods"
}

variable "service_port" {
  default     = "80"
  description = "port the service is reachable at within the cluster"
  type        = number
}

variable "letsencrypt_issuer" {
  type        = string
  description = "issuer to use for certificates of cert-manager"
}
variable "ingress_class" {
  type        = string
  description = "ingress class to specify in ingress"
  default     = "nginx"
}

variable "server_snippet" {
  type        = string
  description = "Server snippet to hand over to Nginx. Useful e.g. to block locations (health, metrics) for all provided services."
  default     = ""
}

variable "host" {
  type = string
}

variable "service_name" {
  type = string
}

variable "path" {
  type    = string
  default = "/"
}

variable "limit_rpm" {
  type        = string
  description = "the requests per minute that influence rate limiting. 0 means deactivated"
  default     = "0"
}

variable "limit_whitelist" {
  type        = string
  description = "comma-separated list of CIDR or IPs"
  default     = ""
}

variable "proxy_body_size" {
  type        = string
  description = "this value might be useful to handle large uploads"
  default     = "1m"
}
