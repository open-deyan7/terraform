
resource "helm_release" "postgresql" {
  name       = "${var.app_name}-postgresql"
  chart      = "postgresql"
  repository = "https://charts.bitnami.com/bitnami"
  namespace  = var.namespace
  version    = var.chart_version

  set {
    name  = "auth.password"
    value = var.database_password
  }

  set {
    name  = "auth.username"
    value = var.database_username
  }
  set {
    name  = "primary.service.ports.postgresql"
    value = var.database_port
  }
  set {
    name  = "auth.replicationPassword"
    value = "repl_password"
  }

  set {
    name  = "auth.database"
    value = var.database_name
  }

  set {
    name  = "metrics.enabled"
    value = var.metrics_enabled
  }


  set {
    name  = "auth.existingSecret"
    value = var.existing_secret
  }

  values = var.helm_values
}


module "postgres-backup-restore" {
  count             = var.backup_aws_enabled ? 1 : 0
  depends_on        = [helm_release.postgresql]
  source            = "git::https://gitlab.com/open-deyan7/terraform.git//postgres_backup_restore?ref=494c7cf019ddad6bed5a4775b589d85d6db17478"
  app_name          = var.app_name
  database_address  = helm_release.postgresql.name
  database_name     = var.database_name
  database_password = var.database_password
  database_user     = var.database_username
  database_port     = var.database_port
  namespace         = var.namespace
  aws_access_key    = var.backup_aws_access_key
  aws_secret_key_id = var.backup_aws_secret_key_id
  aws_region        = var.backup_aws_region
  s3_bucket         = var.backup_s3_bucket

  schedule              = var.backup_schedule
  s3_object             = var.restore_s3_object
  encryption_passphrase = var.backup_encryption_passphrase
}
