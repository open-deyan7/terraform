
variable "database_password" {
  type = string
}
variable "database_name" {
  type = string
}
variable "database_username" {
  type = string
}
variable "namespace" {
  type = string
}


variable "app_name" {
  type        = string
  description = "used to describe the backup job and name the backup files"
}
variable "database_port" {
  default = 5432
  type    = number
}

variable "backup_aws_enabled" {
  description = "Enables or disables the AWS backup functionality for the database"
  type        = bool
  default     = true
}

variable "backup_s3_bucket" {
  type        = string
  description = "s3 bucket for storage of pg dump backup"
}
variable "backup_aws_secret_key_id" {
  type        = string
  description = "access key secret for s3 authentication"
}
variable "backup_aws_access_key" {
  type        = string
  description = "access key id for s3 authentication"
}
variable "backup_aws_region" {
  type        = string
  description = "the region the backup bucket is in"
}
variable "restore_s3_object" {
  type        = string
  description = "if set, this object will be restored to the keycloak database once. Must point to a .dump file created by pg_restore -Fc"
  default     = null
}
variable "backup_encryption_passphrase" {
  type        = string
  description = "the backup is encrypted and restored using aes-256-cbc with this symmetric key"
}
variable "backup_schedule" {
  type        = string
  default     = "0 3 * * *"
  description = "cron expression which describes when to execute the backup"
}

variable "metrics_enabled" {
  type        = bool
  default     = false
  description = "enables Prometheus Exporter"
}

variable "helm_values" {
  type = list(string)
  default = [
    <<YAML
    primary:
      resources:
        limits:
          cpu: 1
          memory: 512Mi
YAML
  ]
}

variable "chart_version" {
  type    = string
  default = "11.9.13"
}


variable "existing_secret" {
  type        = string
  default     = ""
  description = "Existing secret for PostgreSQL authentication"
}