resource "kubernetes_cron_job_v1" "postgres-backup" {
  metadata {
    name      = "${var.app_name}-backup"
    namespace = var.namespace
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 3
    schedule                      = var.schedule
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 2
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 60
        template {
          metadata {}
          spec {

            container {
              name  = "${var.app_name}-backup"
              image = "registry.gitlab.com/open-deyan7/postgres-aws-container:latest"
              command = ["/bin/sh",
                "-c",
                "file=db-backup-$(date +\"%Y-%m-%d-%H-%M\").dump && resource=\"${var.app_name}/$${file}\" && pg_dump -U $DB_USER -h $DB_ADDR -Fc -f $${file} -p $DB_PORT $DB_NAME && echo \"backup saved to file $${file}\" && echo \"encrypting backup file\" && echo \"$ENCRYPTION_PASSPHRASE\" | gpg --batch --yes --passphrase-fd 0 --output backup.enc --symmetric --cipher-algo AES256 $${file} && echo \"encrypting backup file complete\" && aws s3api put-object --bucket ${var.s3_bucket} --key $${resource} --body backup.enc && echo \"upload complete\""
              ]

              env {
                name = "ENCRYPTION_PASSPHRASE"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws-backup-secret.metadata[0].name
                    key  = "encryption_passphrase"
                  }
                }
              }

              env {
                name  = "AWS_DEFAULT_REGION"
                value = var.aws_region
              }

              env {
                name = "AWS_SECRET_ACCESS_KEY"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws-backup-secret.metadata[0].name
                    key  = "aws_secret_key_id"
                  }
                }
              }

              env {
                name = "AWS_ACCESS_KEY_ID"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws-backup-secret.metadata[0].name
                    key  = "aws_access_key"
                  }
                }
              }

              env {
                name = "DB_USER"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws-backup-secret.metadata[0].name
                    key  = "database_user"
                  }
                }
              }

              env {
                name  = "DB_ADDR"
                value = var.database_address
              }

              env {
                name  = "DB_PORT"
                value = var.database_port
              }

              env {
                name  = "DB_NAME"
                value = var.database_name
              }
              env {
                name = "PGPASSWORD"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws-backup-secret.metadata[0].name
                    key  = "database_password"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
