# Postgres Backup and Restore

Will backup a postgres database to s3.

When `s3_object` is set, it will be downloaded and restored to the database.

## Considerings when restoring

See [Postgres Restore Job](../postgres_restore_job/README.md) for considerations