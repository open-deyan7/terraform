
module "restore_job" {
  source                = "git::https://gitlab.com/open-deyan7/terraform.git//postgres_restore_job/?ref=494c7cf019ddad6bed5a4775b589d85d6db17478"
  app_name              = var.app_name
  aws_access_key        = var.aws_access_key
  aws_region            = var.aws_region
  aws_secret_key_id     = var.aws_secret_key_id
  database_address      = var.database_address
  database_name         = var.database_name
  database_password     = var.database_password
  database_user         = var.database_user
  database_port         = var.database_port
  namespace             = var.namespace
  s3_bucket             = var.s3_bucket
  s3_object             = var.s3_object
  encryption_passphrase = var.encryption_passphrase
}

module "backup_job" {
  source            = "git::https://gitlab.com/open-deyan7/terraform.git//postgres_backup_cronjob?ref=494c7cf019ddad6bed5a4775b589d85d6db17478"
  app_name          = var.app_name
  aws_access_key    = var.aws_access_key
  aws_region        = var.aws_region
  aws_secret_key_id = var.aws_secret_key_id
  database_address  = var.database_address
  database_name     = var.database_name
  database_password = var.database_password
  database_user     = var.database_user
  database_port     = var.database_port
  namespace         = var.namespace
  s3_bucket         = var.s3_bucket
  schedule          = var.schedule

  encryption_passphrase = var.encryption_passphrase
}
