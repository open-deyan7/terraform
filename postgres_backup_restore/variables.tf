variable "namespace" {
  type = string
}
variable "app_name" {
  type        = string
  description = "used to describe the backup job and name the backup files"
}
variable "schedule" {
  type        = string
  default     = "0 3 * * *"
  description = "cron expression which describes when to execute the backup"
}
variable "database_address" {
  type = string
}
variable "database_user" {
  type = string
}
variable "database_port" {
  default = 5432
  type    = number
}
variable "database_name" {
  type = string
}
variable "database_password" {
  type = string
}
variable "s3_bucket" {
  type        = string
  description = "s3 bucket for storage of pg dump backup"
}
variable "aws_secret_key_id" {
  type        = string
  description = "access key secret for s3 authentication"
}
variable "aws_access_key" {
  type        = string
  description = "access key id for s3 authentication"
}
variable "aws_region" {
  type        = string
  description = "the region the backup bucket is in"
}
variable "s3_object" {
  type        = string
  description = "the key of the backup file to restore"
  default     = null
}
variable "encryption_passphrase" {
  type        = string
  description = "the backup is decrypted using aes-256-cbc with this symmetric key"
}
