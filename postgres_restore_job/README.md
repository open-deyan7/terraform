# Postgres Restore

When `s3_object` is set, the backup created by postgres_backup_restore or by `pg_dump -Fc` will be downloaded and restored to the database.

## Considerings when restoring

The restore job is only executed, when the specification (including the name of the s3 object) changes. Nevertheless we
consider it a best practice to remove the s3 object value once restoration has been complete. This will prevent the job
from executing when the module is updated and similar.

The database is still accepting connections while restoring, so there should not be traffic on the system (or you should restore a different database and then change database configuration)