
resource "kubernetes_secret" "aws-restore-secret" {
  metadata {
    name      = "${var.app_name}-aws-restore-secret"
    namespace = var.namespace
  }

  data = {
    aws_access_key        = (var.aws_access_key)
    aws_secret_key_id     = (var.aws_secret_key_id)
    encryption_passphrase = var.encryption_passphrase
    database_password     = var.database_password
    database_user         = var.database_user
  }
}