
resource "kubernetes_job" "postgres-keycloak-restore" {
  count = var.s3_object != null ? 1 : 0
  metadata {

    name      = "${var.app_name}-restore"
    namespace = var.namespace
  }

  wait_for_completion = true
  timeouts {
    create = var.restore_timeout
    update = var.restore_timeout
  }

  spec {

    backoff_limit = 2
    template {
      spec {

        container {
          name  = "${var.app_name}-restore"
          image = "registry.gitlab.com/open-deyan7/postgres-aws-container:latest"
          command = ["/bin/sh",
            "-c",
            "echo \"download backup file\" && aws s3api get-object --bucket ${var.s3_bucket} --key ${var.s3_object} backup-encrypted.dump && echo \"download complete, starting decryption\" && echo \"$ENCRYPTION_PASSPHRASE\" | gpg --batch --yes --passphrase-fd 0 --output backup.dump --decrypt backup-encrypted.dump && echo \"decryption of backup file complete\"  && pg_restore --clean -U $DB_USER -h $DB_ADDR -p $DB_PORT --dbname $DB_NAME backup.dump && echo \"restore complete\""
          ]

          env {
            name = "ENCRYPTION_PASSPHRASE"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.aws-restore-secret.metadata[0].name
                key  = "encryption_passphrase"
              }
            }
          }
          env {
            name  = "AWS_DEFAULT_REGION"
            value = var.aws_region
          }

          env {
            name = "AWS_SECRET_ACCESS_KEY"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.aws-restore-secret.metadata[0].name
                key  = "aws_secret_key_id"
              }
            }
          }

          env {
            name = "AWS_ACCESS_KEY_ID"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.aws-restore-secret.metadata[0].name
                key  = "aws_access_key"
              }
            }
          }

          env {
            name = "DB_USER"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.aws-restore-secret.metadata[0].name
                key  = "database_user"
              }
            }
          }

          env {
            name  = "DB_ADDR"
            value = var.database_address
          }

          env {
            name  = "DB_PORT"
            value = var.database_port
          }

          env {
            name  = "DB_NAME"
            value = var.database_name
          }
          env {
            name = "PGPASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.aws-restore-secret.metadata[0].name
                key  = "database_password"
              }
            }
          }
        }
      }
      metadata {}
    }
  }
}
