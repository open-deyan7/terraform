# Renovate - Automatic dependency updates
This will deploy https://github.com/renovatebot/renovate to your cluster.

## Deployment
Supply the mandatory variables and deploy. At the specified interval, a cron job will execute Renovate.

## Configuration
The repos themselves need to be configured via a `renovate.json` file that is commited to the repos. Our configuration suggestions can be found as JSON files in this repo.