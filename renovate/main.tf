resource "kubernetes_namespace" "renovate" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_cron_job_v1" "renovate_bot" {
  metadata {
    name      = "renovate-bot"
    namespace = kubernetes_namespace.renovate.metadata[0].name
  }

  spec {
    schedule           = var.schedule
    concurrency_policy = "Forbid"

    job_template {
      metadata {}

      spec {
        template {
          metadata {}
          spec {
            volume {
              name = "config-volume"

              config_map {
                name = "renovate-config"
              }
            }
            volume {
              name = "work-volume"
              empty_dir {}
            }
            container {
              name  = "renovate-bot"
              image = "renovate/renovate:${var.renovate_version}"
              args  = var.repos_to_scan
              env_from {
                secret_ref {
                  name = "renovate-env"
                }
              }

              volume_mount {
                name       = "config-volume"
                mount_path = "/opt/renovate/"
              }

              volume_mount {
                name       = "work-volume"
                mount_path = "/tmp/renovate/"
              }

              resources {
                limits = {
                  cpu    = var.resources.limits.cpu
                  memory = var.resources.limits.memory
                }
                requests = {
                  cpu    = var.resources.requests.cpu
                  memory = var.resources.requests.memory
                }
              }
            }

            restart_policy = "Never"
          }
        }
      }
    }
  }
}

resource "kubernetes_secret" "renovate_env" {
  metadata {
    name      = "renovate-env"
    namespace = kubernetes_namespace.renovate.metadata[0].name
  }

  data = merge(var.secret_extras, {
    RENOVATE_TOKEN    = var.token
    RENOVATE_PLATFORM = var.platform
    RENOVATE_USERNAME = var.username
    GITHUB_COM_TOKEN  = var.github_com_token
  })
}

resource "kubernetes_config_map" "renovate_config" {
  metadata {
    name      = "renovate-config"
    namespace = kubernetes_namespace.renovate.metadata[0].name
  }

  data = {
    "config.json" = var.config_json
  }
}

