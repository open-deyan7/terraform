variable "schedule" {
  type        = string
  description = "When the cron job should run"
  default     = "@hourly"
}

variable "namespace" {
  type        = string
  description = "The namespace which will be created and where Renovate will run"
  default     = "renovate"
}

variable "renovate_version" {
  type        = string
  description = "The version of Renovate to use"
  default     = "34.157.1"
}

variable "repos_to_scan" {
  description = "The location of git repos that should be scanned. In the case of gitlab, these can look like 'deyan7/components/backend'"
  type        = list(string)
}

variable "resources" {
  description = "The Kubernetes resource requests and limits Renovate will use"
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "2000m"
      memory = "600Mi"
    }
    requests = {
      cpu    = "100m"
      memory = "200Mi"
    }
  }
}

variable "token" {
  type        = string
  description = "In the case of Gitlab, this is a Personal Access Token or Group Access Token"
}

variable "username" {
  type        = string
  description = "This is the username for logging in together with the renovate_token"
}

variable "github_com_token" {
  type        = string
  description = "This is a Personal Access Token on github.com which Renovate uses to fetch e.g. changelogs"
  default     = ""
}

variable "platform" {
  type    = string
  default = "gitlab"
  validation {
    condition     = contains(["azure", "bitbucket", "bitbucket-server", "codecommit", "gitea", "github", "gitlab"], var.platform)
    error_message = "Value did not match any of [azure, bitbucket, bitbucket-server, codecommit, gitea, github, gitlab]"
  }
}

variable "config_json" {
  type        = string
  description = "This is the Kubernetes Configmap content that Renovate will use"
  default     = "{}"
}

variable "secret_extras" {
  type        = map(string)
  description = "The contents of this map will be merged with the Kubernetes secret that will be used by Renovate"
  default     = {}
}
