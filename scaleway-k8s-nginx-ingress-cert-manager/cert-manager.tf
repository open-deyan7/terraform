

resource "helm_release" "cert_manager" {
  name      = "cert-manager"
  namespace = kubernetes_namespace.cert-manager.metadata[0].name

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.7.1"
  values = [
    <<YAML
installCRDs: true
startupapicheck.timeout: 10m
YAML
  ]
}
