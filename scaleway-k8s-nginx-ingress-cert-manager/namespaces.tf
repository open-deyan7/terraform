resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
  }
}
resource "kubernetes_namespace" "nginx" {
  metadata {
    name = "nginx"
  }
}
