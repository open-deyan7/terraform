resource "helm_release" "nginx_ingress" {
  name      = "nginx-ingress"
  namespace = kubernetes_namespace.nginx.metadata[0].name

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  values = [
    <<YAML
    controller:
      service:
        loadBalancerIP: ${var.ingress_load_balancer_ipv4}
        annotations:
          service.beta.kubernetes.io/scw-loadbalancer-proxy-protocol-v2: true
          service.beta.kubernetes.io/scw-loadbalancer-use-hostname: true
        externalTrafficPolicy: Local #avoid node forwarding
      metrics:
        enabled: ${var.metrics_enabled}
        service:
          annotations:
            prometheus.io/path: /metrics
            prometheus.io/port: "10254"
            prometheus.io/scrape: "${var.metrics_enabled}"
      config:
        use-proxy-protocol: true #enable proxy protocol to get client ip addr instead of loadbalancer one
    YAML
  ]
}

