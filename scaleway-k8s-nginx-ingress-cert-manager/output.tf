
output "letsencrypt_issuer_production" {
  value = "letsencrypt-prod"
}
output "letsencrypt_issuer_staging" {
  value = "letsencrypt-staging"
}