variable "letsencrypt_email" {
  type        = string
  description = "email address used when registring letsencrypt certificate. This email will receive expiration notifications."
}
variable "ingress_load_balancer_ipv4" {
  type        = string
  description = "ip of the load balancer of scaleway"
}

variable "metrics_enabled" {
  type        = bool
  default     = false
  description = "enables the Nginx Ingress Controller metrics"
}
