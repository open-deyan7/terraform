# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version     = "3.2.2"
  constraints = ">= 3.0.0, < 4.0.0"
  hashes = [
    "h1:vWAsYRd7MjYr3adj8BVKRohVfHpWQdvkIwUQ2Jf5FVM=",
    "zh:3248aae6a2198f3ec8394218d05bd5e42be59f43a3a7c0b71c66ec0df08b69e7",
    "zh:32b1aaa1c3013d33c245493f4a65465eab9436b454d250102729321a44c8ab9a",
    "zh:38eff7e470acb48f66380a73a5c7cdd76cc9b9c9ba9a7249c7991488abe22fe3",
    "zh:4c2f1faee67af104f5f9e711c4574ff4d298afaa8a420680b0cb55d7bbc65606",
    "zh:544b33b757c0b954dbb87db83a5ad921edd61f02f1dc86c6186a5ea86465b546",
    "zh:696cf785090e1e8cf1587499516b0494f47413b43cb99877ad97f5d0de3dc539",
    "zh:6e301f34757b5d265ae44467d95306d61bef5e41930be1365f5a8dcf80f59452",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:913a929070c819e59e94bb37a2a253c228f83921136ff4a7aa1a178c7cce5422",
    "zh:aa9015926cd152425dbf86d1abdbc74bfe0e1ba3d26b3db35051d7b9ca9f72ae",
    "zh:bb04798b016e1e1d49bcc76d62c53b56c88c63d6f2dfe38821afef17c416a0e1",
    "zh:c23084e1b23577de22603cff752e59128d83cfecc2e6819edadd8cf7a10af11e",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.38.3"
  constraints = ">= 2.2.0, < 3.0.0"
  hashes = [
    "h1:SgW4NE8SXdqX3qGcxyZOeo61G3Ri0xkqpelhrhE+V4c=",
    "zh:18b5f46c292ad9037656979685c296429d6ea3bb4f437d60148d9c4db389b957",
    "zh:44bb07e4a788137939838b8e53afab1ecf6ffe70623b42e4c45bff2c6a67c730",
    "zh:456af32c19acdd5e3b1d2bb98fabc1fe8c1eaee198b098ba2cfbb19b2de12c81",
    "zh:77a5311a1603b07c553f24bc2faf8e0a94411452f23eef88ffaa47b7a0e999a2",
    "zh:825bb990d0279eb5ac7798e91fd04d2bf39f5cc55e6d05d07e382e9b64044232",
    "zh:8de81e03028d8af19bfc81e5ba9112606eeefbb3f3039acf781964c3473b8cd2",
    "zh:ac5ed6419e1702c75838f2022c6b38a93ecb0d7a8406a44b745df8776c21492c",
    "zh:b2eb22b548ac011b65a6bf7d6a145479405eea934458b6d949685f75a65ec34e",
    "zh:bb23ca3494747a0c8ceb8b237020d87f22363f731710c8100b6d2392f5c40de0",
    "zh:bcd89064f3e921a24dc5714d09b0a1d2ea015327e93425e8d6d1b6af7589f6bc",
    "zh:c2b31bad9895dc55dc091df2b1b36c4fbddb43367bb435797f9e51189188ec1d",
    "zh:c84e38e195d035e3ad28d020dfc2f9d4d359e140c1fadedbaea3cffe64d3f89a",
    "zh:d66fcaae96c59d5d36d373f5a52c860d7ff90a95f0e2e623e90b9a2eca6371fa",
    "zh:dfa4ab439fe9813fa08bae22f0090c7b80bad1d76023bcf263d0c321acf59dfc",
  ]
}
