# Scaleway kubernetes cluster

## Sending SMTP from scaleway servers

If you want to send emails from scaleway servers using SMTP, you need to configure the security group of the node pool to allow snmp.

Unfortunately, the scaleway provider does not allow to modify the default security group on creation.

That is why you need to add the security group after it has been created by scaleway using `terraform import`

1. Go to scaleway project / instances / security groups / click on the title of the only security group
2. Copy the **<region>/<security-group-id>** part of the url
3. Add the following to your terraform files

```terraform
resource "scaleway_instance_security_group" "default-security-group" {
  external_rules          = false
  stateful                = false
  enable_default_security = false
}
```

4. run `terraform import scaleway_instance_security_group.default-security-group <region>/<security-group-id>`

The security group is now being managed by your terraform state and on the next apply, it will enable the SMTP option.

**Please be aware that you need to have at least one identified user in the scaleway account (passport / identity card). The call will fail otherwise.**