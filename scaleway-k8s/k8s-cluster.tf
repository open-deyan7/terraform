
resource "scaleway_k8s_cluster" "primary" {
  name                        = var.cluster_name
  version                     = var.k8s_version
  region                      = var.cluster_region
  cni                         = var.cni
  delete_additional_resources = false
  private_network_id          = scaleway_vpc_private_network.private_network.id

  auto_upgrade {
    enable                        = true
    maintenance_window_day        = "sunday"
    maintenance_window_start_hour = 3
  }
}

resource "scaleway_k8s_pool" "primary-pool" {
  cluster_id  = scaleway_k8s_cluster.primary.id
  name        = "primary-pool"
  node_type   = var.node_type
  size        = var.node_pool_size
  autohealing = "true"
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.primary-pool] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.primary.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.primary.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster.primary.kubeconfig[0].cluster_ca_certificate
    kubeconfig             = scaleway_k8s_cluster.primary.kubeconfig[0].config_file
  }
}

resource "scaleway_lb_ip" "nginx_ip" {
}

resource "scaleway_vpc_private_network" "private_network" {
  name = "private-network"
}
