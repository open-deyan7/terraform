output "host" {
  value = null_resource.kubeconfig.triggers.host
}
output "token" {
  value = null_resource.kubeconfig.triggers.token
}
output "cluster_ca_certificate" {
  value = base64decode(
    null_resource.kubeconfig.triggers.cluster_ca_certificate
  )
}
output "kubeconfig" {
  value = null_resource.kubeconfig.triggers.kubeconfig
}
output "ingress_load_balancer_ipv4" {
  value = scaleway_lb_ip.nginx_ip.ip_address
}