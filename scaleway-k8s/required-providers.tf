
terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    null = {
      source  = "hashicorp/null"
      version = ">= 3.0.0, < 4.0.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = ">= 2.2.0, < 3.0.0"
    }
  }
}
