variable "cni" {
  type        = string
  default     = "flannel"
  description = "cluster network interface to choose"
}
variable "k8s_version" {
  type        = string
  default     = "1.24"
  description = "only give version in form of x.y, minor versions will be updated automatically when updates are available"
}
variable "cluster_name" {
  type    = string
  default = "primary-cluster"
}
variable "cluster_region" {
  type        = string
  description = "region the cluster is created in. Possible values are fr-par, nl-ams, pl-waw"
  default     = "nl-ams"
}
variable "node_type" {
  type        = string
  default     = "DEV1-M"
  description = "node type to use"
}
variable "node_pool_size" {
  type        = number
  description = "number of nodes in the cluster node pool"
}
