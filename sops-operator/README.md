# SOPS Operator
## Apply Age key secret manually
To be able for SOPS Operator to work, first you´ll need to manually apply a secret containing the private age key as shown in `sops-secret.example.yml` to the cluster.  

**Make sure this secret never gets commited to git or persisted in your Terraform project!**