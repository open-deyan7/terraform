resource "helm_release" "sops-secrets-operator" {
  name             = "sops-secrets-operator"
  chart            = "sops-secrets-operator"
  repository       = "https://isindir.github.io/sops-secrets-operator/"
  namespace        = "${var.namespace_prefix}${var.environment_name}"
  version          = var.sops_secrets_operator_version
  create_namespace = var.create_namespace

  values = [
    <<EOF
extraEnv:
- name: SOPS_AGE_KEY_FILE
  value: /etc/sops-age-key-file/key
secretsAsFiles:
- mountPath: /etc/sops-age-key-file
  name: sops-age-key-file
  secretName: sops-age-key-file
metrics:
  enabled: ${var.metrics_enabled}
EOF
  ]
}




