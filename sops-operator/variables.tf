variable "environment_name" {
  type = string
}

variable "namespace_prefix" {
  type = string
}

variable "sops_secrets_operator_version" {
  type    = string
  default = "0.8.1"
}

variable "create_namespace" {
  type    = bool
  default = false
}

variable "metrics_enabled" {
  type    = bool
  default = false
}