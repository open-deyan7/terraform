resource "aws_s3_bucket" "terraform_state_backup_bucket" {
  bucket = "${var.product_name}-cluster-${var.environment_name}-terraform-state-backup-bucket"
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "terraform_state_backup_bucket_versioning" {
  bucket = aws_s3_bucket.terraform_state_backup_bucket.bucket
  versioning_configuration {
    status = "Enabled"
  }
}

resource "random_password" "terraform_state_password" {
  keepers = { "version" : "1" }
  length  = 32
}

module "terraform_state_backup_cluster" {
  source                = "git::https://gitlab.com/open-deyan7/terraform.git//terraform_state_backup_cronjob?ref=b00b07b68a45dc43799175950e8034e8b6818ad9"
  namespace             = var.terraform_state_namespace
  encryption_passphrase = random_password.terraform_state_password.result
  s3_folder             = "${var.product_name}-cluster-${var.environment_name}-terraform-state-backup"
  s3_bucket             = aws_s3_bucket.terraform_state_backup_bucket.bucket
  aws_region            = var.aws_config.region
  aws_secret_key_id     = var.aws_config.secret_key_id
  aws_access_key        = var.aws_config.access_key
  k8s_host              = var.k8s_config.host
  k8s_token             = var.k8s_config.token
  k8s_cluster_ca_cert   = var.k8s_config.ca_cert
  k8s_secret_name       = "tfstate-default-${var.k8s_config.secret_suffix_cluster}"
}

module "terraform_state_backup_workload" {
  source                = "git::https://gitlab.com/open-deyan7/terraform.git//terraform_state_backup_cronjob?ref=b00b07b68a45dc43799175950e8034e8b6818ad9"
  namespace             = var.terraform_state_namespace
  encryption_passphrase = random_password.terraform_state_password.result
  s3_folder             = "${var.product_name}-cluster-${var.environment_name}-terraform-state-backup"
  s3_bucket             = aws_s3_bucket.terraform_state_backup_bucket.bucket
  aws_region            = var.aws_config.region
  aws_secret_key_id     = var.aws_config.secret_key_id
  aws_access_key        = var.aws_config.access_key
  k8s_host              = var.k8s_config.host
  k8s_token             = var.k8s_config.token
  k8s_cluster_ca_cert   = var.k8s_config.ca_cert
  k8s_secret_name       = "tfstate-default-${var.k8s_config.secret_suffix_workload}"
}
