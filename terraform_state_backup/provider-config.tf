provider "aws" {
  region     = var.aws_config.region
  access_key = var.aws_config.access_key
  secret_key = var.aws_config.secret_key_id
}
