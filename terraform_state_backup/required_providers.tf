terraform {
  required_version = ">= 1.1.6, < 2.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.4.0, < 5.0.0"
    }
    random = ">= 3.4.3, < 4.0.0"
  }
}
