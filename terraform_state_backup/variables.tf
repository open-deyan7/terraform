variable "product_name" {
  type = string
}

variable "environment_name" {
  type = string
}

variable "terraform_state_namespace" {
  type = string
}

variable "aws_config" {
  type = object({
    region : string
    secret_key_id : string
    access_key : string
  })
}

variable "k8s_config" {
  type = object({
    host : string
    token : string
    ca_cert : string
    secret_suffix_cluster : string
    secret_suffix_workload : string
  })
}
