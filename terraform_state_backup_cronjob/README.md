## Decrypt
To decrypt the state file after downloading from S3, run:

```
echo $ENCRYPTION_PASSPHRASE | gpg --batch --yes --passphrase-fd 0 --decrypt FILE_TO_BE_DECRYPTED | base64 -d | gunzip
```