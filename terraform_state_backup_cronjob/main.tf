resource "kubernetes_cron_job_v1" "tfstate_backup" {
  metadata {
    name      = "${var.k8s_secret_name}-backup"
    namespace = var.namespace
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 3
    schedule                      = var.schedule
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 2
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 60
        template {
          metadata {}
          spec {

            container {
              name  = "${var.k8s_secret_name}-backup"
              image = "alpine:3.17"
              command = ["/bin/sh",
                "-c",
                <<SHCMD
                apk add aws-cli gpg gpg-agent && \
                echo "https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && \
                apk add kubectl && \
                echo $KUBE_CLUSTER_CA_CERT_DATA | base64 -d > ca_cert && \
                echo "###### Wrote Cluster CA Cert" && \
                kubectl -s $KUBE_HOST --token=$KUBE_TOKEN --certificate-authority=ca_cert -n ${var.namespace} get secret ${var.k8s_secret_name} --template={{.data.tfstate}} > ${var.k8s_secret_name}.gz.b64 && \
                echo "###### Wrote state secret" && \
                echo $ENCRYPTION_PASSPHRASE | gpg --batch --yes --passphrase-fd 0 --output ${var.k8s_secret_name}.gz.b64.enc --symmetric --cipher-algo AES256 ${var.k8s_secret_name}.gz.b64 && \
                echo "###### Encrypted state secret" && \
                aws s3api put-object --bucket ${var.s3_bucket} --key ${var.s3_folder}/${var.k8s_secret_name}.gz.b64.enc --body ${var.k8s_secret_name}.gz.b64.enc && \
                echo "###### Uploaded encrypted state secret" && \
                echo "###### BACKUP SUCCESSFUL! ######"
                SHCMD
              ]

              env {
                name = "ENCRYPTION_PASSPHRASE"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws_backup_secret.metadata[0].name
                    key  = "encryption_passphrase"
                  }
                }
              }

              env {
                name  = "AWS_DEFAULT_REGION"
                value = var.aws_region
              }

              env {
                name = "AWS_SECRET_ACCESS_KEY"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws_backup_secret.metadata[0].name
                    key  = "aws_secret_key_id"
                  }
                }
              }

              env {
                name = "AWS_ACCESS_KEY_ID"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.aws_backup_secret.metadata[0].name
                    key  = "aws_access_key"
                  }
                }
              }
              env {
                name = "KUBE_HOST"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.kubernetes_backup_secret.metadata[0].name
                    key  = "k8s_host"
                  }
                }
              }
              env {
                name = "KUBE_TOKEN"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.kubernetes_backup_secret.metadata[0].name
                    key  = "k8s_token"
                  }
                }
              }
              env {
                name = "KUBE_CLUSTER_CA_CERT_DATA"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.kubernetes_backup_secret.metadata[0].name
                    key  = "k8s_cluster_ca_cert"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
