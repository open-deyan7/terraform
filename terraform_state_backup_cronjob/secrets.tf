
resource "kubernetes_secret" "aws_backup_secret" {
  metadata {
    name      = "${var.k8s_secret_name}-aws-backup-secret"
    namespace = var.namespace
  }

  data = {
    aws_access_key        = (var.aws_access_key)
    aws_secret_key_id     = (var.aws_secret_key_id)
    encryption_passphrase = var.encryption_passphrase
  }
}

resource "kubernetes_secret" "kubernetes_backup_secret" {
  metadata {
    name      = "${var.k8s_secret_name}-k8s-backup-secret"
    namespace = var.namespace
  }

  data = {
    k8s_host            = var.k8s_host
    k8s_token           = var.k8s_token
    k8s_cluster_ca_cert = var.k8s_cluster_ca_cert
  }
}
