variable "namespace" {
  type        = string
  description = "The namespace where the secret lives that should be backed up"
}

variable "schedule" {
  type        = string
  default     = "0 4 * * *"
  description = "cron expression which describes when to execute the backup"
}

variable "s3_folder" {
  type        = string
  description = "used to name the S3 folder"
}

variable "s3_bucket" {
  type        = string
  description = "s3 bucket for storage of pg dump backup"
}
variable "aws_secret_key_id" {
  type        = string
  description = "access key secret for s3 authentication"
}
variable "aws_access_key" {
  type        = string
  description = "access key id for s3 authentication"
}
variable "aws_region" {
  type        = string
  description = "the region the backup bucket is in"
}
variable "encryption_passphrase" {
  type        = string
  description = "the backup is encrypted using aes-256-cbc with this symmetric key"
}

variable "k8s_host" {
  type        = string
  description = "The Kubernetes host and port to use for fetching the secret"
}

variable "k8s_token" {
  type        = string
  description = "The token of the service account that is used to connect to the cluster"
}

variable "k8s_cluster_ca_cert" {
  type        = string
  description = "The cert of the cluster to establish TLS verification"
}

variable "k8s_secret_name" {
  type        = string
  description = "The name of the secret to be backed up"
}
