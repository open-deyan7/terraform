resource "kubernetes_role" "terraform_state" {
  metadata {
    name      = "terraform-state"
    namespace = kubernetes_namespace.state_namespace.metadata[0].name
  }
  rule {
    api_groups = [""]
    resources  = ["secrets"]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "patch",
      "update",
    ]
  }

  rule {
    api_groups = ["coordination.k8s.io"]
    resources  = ["leases"]
    verbs      = ["*"]
  }
}

resource "kubernetes_service_account" "terraform_state" {
  metadata {
    name      = "terraform-state"
    namespace = kubernetes_namespace.state_namespace.metadata[0].name
  }
}

resource "kubernetes_role_binding" "terraform_state" {
  metadata {
    name      = "terraform-state"
    namespace = kubernetes_namespace.state_namespace.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.terraform_state.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.terraform_state.metadata[0].name
    namespace = kubernetes_namespace.state_namespace.metadata[0].name
  }
}

# This secret will be autofilled by Kubernetes with a token. Newer Kubernetes versions do not autogenerate long-living secrets for ServiceAccounts anymore
resource "kubernetes_secret" "terraform_state_serviceaccount_secret" {
  metadata {
    name      = "terraform-state-serviceaccount-secret"
    namespace = kubernetes_namespace.state_namespace.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account.terraform_state.metadata[0].name
    }
  }
  type = "kubernetes.io/service-account-token"
}
